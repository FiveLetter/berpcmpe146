/*
 * spi1.h
 *
 *  Created on: Feb 28, 2016
 *      Author: fiveletter
 */

#ifndef L2_DRIVERS_SPI1_H_
#define L2_DRIVERS_SPI1_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

typedef struct spi_status
{
    bool page_size  :1;
    bool protect    :1;
    uint8_t density :4;
    bool comp       :1;
    bool busy1      :1;
    bool es         :1;
    bool ps1        :1;
    bool ps2        :1;
    bool sle        :1;
    bool res0       :1;
    bool epe        :1;
    bool res1       :1;
    bool busy2      :1;
}spi_status_S;

/**
 * Initializes SPI 1 and corresponding pinds
 * @return true if initialization succeeded.
 */
bool spi1_init(void);

/**
 * Transfers a byte and receives a byte from SPI 1 data register
 * @param out the value that will be sent to the spi device
 * @return the byte of information that is passed back through data register
 */
uint8_t spi1_byte_transfer(uint8_t out);

/**
 * Selects the spi1
 * @param gpio_pin enumeration of specific GPIO pin
 */
void spi1_select(void);

/**
 * De selects the spi1
 * @param gpio_pin enumeration of specific GPIO pin
 */
void spi1_de_select(void);

void spi1_get_status_bits(void);

#ifdef __cplusplus
}
#endif
#endif /* L2_DRIVERS_SPI1_H_ */
