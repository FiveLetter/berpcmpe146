/*
 * uart1.h
 *
 *  Created on: Mar 7, 2016
 *      Author: fiveletter
 */

#ifndef L2_DRIVERS_UART1_H_
#define L2_DRIVERS_UART1_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

const uint32_t PCLK = 48000000;

/**
 * Initializes UART 1 and corresponding pins
 * @return true if initialization succeeded.
 */
bool uart1_init(uint32_t baud);

/**
 * Transfers a byte from UART 1 data register
 * @param out the value that will be sent to the uart device
 */
bool uart1_putchar(uint8_t out);

/**
 * Receive a byte from UART 1 data register
 * @return data from UART 1 data register
 */
bool uart1_getchar(uint8_t *out);

#ifdef __cplusplus
}
#endif
#endif /* L2_DRIVERS_UART1_H_ */
