/*
 * uart1.c
 *
 *  Created on: Mar 7, 2016
 *      Author: fiveletter
 */

#include "uart1.h"
#include "LPC17xx.h"
#include <stdio.h>
#include <stdbool.h>

static bool already_initialized = false;

static bool set_baud_rate(uint32_t baud) {
  bool success = true;

  LPC_UART1->LCR = (1 << 7);          ///< Set Dlab bit
  uint16_t bd = (PCLK / (16 * baud)) + 0.5;   ///< Calculate baud rate

  LPC_UART1->DLL = (bd >> 0);         ///< Set Lower
  LPC_UART1->DLM = (bd >> 8);         ///< Set Upper

  LPC_UART1->LCR = (3 << 0);          ///< Set transfer to 8-bit and disable Dlab

  return success;
}

bool uart1_init(uint32_t baud)
{
  if (already_initialized) {
    printf("Already initialized");
    return true;
  }

  LPC_SC->PCONP |= (1 << 4);            ///< Enable PCUART1
  LPC_SC->PCLKSEL0 &= ~( 3 << 8);         ///< Reset pclock divider pins
  LPC_SC->PCLKSEL0 |= (1 << 8);         ///< Set pclock divider to 1

  LPC_PINCON->PINSEL4 &= ~( 3 << 0 | 3 << 2);   ///< Clear pins of current value
  LPC_PINCON->PINSEL4 |= (2 << 0 | 2 << 2);     ///< Enable TXDX1 and RXDX1

  set_baud_rate(baud);

  already_initialized = true;
  return already_initialized;
}

bool uart1_putchar(uint8_t out)
{
  bool success = true;

  LPC_UART1->THR = out;
  while(!(LPC_UART1->LSR & (1 << 6)));  ///< Waiting for THR to become empty

  return success;
}

bool uart1_getchar(uint8_t *out) {
  bool success = true;

  while(!(LPC_UART1->LSR & (1 << 0)));
  *out = LPC_UART1->RBR;

  return success;
}
