/*
 * sp1.c
 *
 *  Created on: Feb 28, 2016
 *      Author: fiveletter
 */

#include "spi1.h"
#include "LPC17xx.h"
#include "gpio.h"
#include <stdio.h>
#include <stdbool.h>

static bool is_init = false;

bool spi1_init(void)
{
    bool successful = true;
    printf("Initializing SPI!\n");

    // Power and clock setup
    LPC_SC->PCONP |= (1<<10);
    LPC_SC->PCLKSEL0 &= ~(3<<20); //< clear current state
    LPC_SC->PCLKSEL0 |= (1<<20); // set state

    // Setup MISO, MOSI, and SCK pins
    LPC_PINCON->PINSEL0 &= ~(3 << 14 | 3 << 16 | 3 << 18); //< clear current state
    LPC_PINCON->PINSEL0 |= (2 << 14 | 2 << 16 | 2 << 18); //< set state

    // Setup spi options
    LPC_SSP1->CR0 = 7;          ///< 8 bit frames
    LPC_SSP1->CR1 = (1 << 1);   ///< Set as Master
    LPC_SSP1->CPSR = 8;         ///< SCK Speed/8

    is_init = successful;
    return successful;
}

uint8_t spi1_byte_transfer(uint8_t out)
{
    LPC_SSP1->DR = out;
    while(LPC_SSP1->SR & (1 << 4));
    return LPC_SSP1->DR;
}

void spi1_select(void)
{
    gpio_clear(GPIO_SPI1_CS);
}

void spi1_de_select(void)
{
    gpio_set(GPIO_SPI1_CS);
}

void spi1_get_status_bits(void)
{
    uint16_t status_holder = 0;
    spi_status_S status = {0};
    if (!is_init) {
        printf("SPI 1 is not initialized!\n");
    }

    spi1_select();
    spi1_byte_transfer(0xD7);
    status_holder |= spi1_byte_transfer(0xff);
    status_holder |= spi1_byte_transfer(0xff) << 8;
    spi1_de_select();

    memcpy(&status, &status_holder, (size_t) 2);

    printf("Page Size:\t %x\n", status.page_size);
    printf("Protect:\t %x\n", status.protect);
    printf("Density:\t %x\n", status.density);
    printf("comp:\t %x\n", status.comp);
    printf("busy1:\t %x\n", status.busy1);
    printf("es:\t %x\n", status.es);
    printf("ps1:\t %x\n", status.ps1);
    printf("ps2:\t %x\n", status.ps2);
    printf("sle:\t %x\n", status.sle);
    printf("res0:\t %x\n", status.res0);
    printf("epe:\t %x\n", status.epe);
    printf("res1:\t %x\n", status.res1);
    printf("busy2:\t %x\n", status.busy2);
}
