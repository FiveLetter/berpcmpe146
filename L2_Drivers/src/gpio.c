/*
 * gpio.c
 *
 *  Created on: Feb 15, 2016
 *      Author: fiveletter
 */
#include "LPC17xx.h"
#include "gpio.h"

#define ARRAY_SIZEOF(a) (sizeof(a)/sizeof(*(a)))

// dir 0 output, 1 input
const gpio_info_S gpio_config[] = {
    { .port = 1, .pin = 0, .dir = 0}, ///< BOARD LED 9
	{ .port = 0, .pin = 29, .dir = 1}, ///< SWITCH IN
	{ .port = 0, .pin = 1, .dir = 0}, /// BREAD LED
	{ .port = 1, .pin = 8, .dir = 0}, ///< BOARD LED 6
	{ .port = 0, .pin = 6, .dir = 0}, ///< SPI1 Chip Select
};

static LPC_GPIO_TypeDef* gpio_get_gpio_pointer(gpio_info_S config_info)
{
	switch (config_info.port) {
	case 0:
		return LPC_GPIO0;
		break;
	case 1:
		return LPC_GPIO1;
		break;
	case 2:
		return LPC_GPIO2;
		break;
	default:
		break;
	}
}


static void gpio_config_pin(gpio_info_S config_info)
{
	LPC_GPIO_TypeDef *gpio_reg = gpio_get_gpio_pointer(config_info);

	if (config_info.dir == 1) {
		gpio_reg->FIODIR &= ~(1 << config_info.pin); ///< input
	} else {
		gpio_reg->FIODIR |= (1 << config_info.pin); ///< output
	}
}

/**
 * Initializes the gpio peripheral
 */
void gpio_init(void)
{
    for (int i = 0; i < ARRAY_SIZEOF(gpio_config); i++)
    {
    	gpio_config_pin(gpio_config[i]);
    }
}

void gpio_set(gpio_pin_E gpio)
{
	gpio_info_S config_info = gpio_config[gpio];
	LPC_GPIO_TypeDef *gpio_reg = gpio_get_gpio_pointer(config_info);

	gpio_reg->FIOSET |= (1 << config_info.pin);
}

void gpio_clear(gpio_pin_E gpio)
{
	gpio_info_S config_info = gpio_config[gpio];
	LPC_GPIO_TypeDef *gpio_reg = gpio_get_gpio_pointer(config_info);

	gpio_reg->FIOCLR |= (1 << config_info.pin);
}

bool gpio_state(gpio_pin_E gpio)
{
	gpio_info_S config_info = gpio_config[gpio];
	LPC_GPIO_TypeDef *gpio_reg = gpio_get_gpio_pointer(config_info);

	return (gpio_reg->FIOPIN & (1<<config_info.pin));
}
