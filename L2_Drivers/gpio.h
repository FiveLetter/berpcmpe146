/*
 * gpio.h
 *
 *  Created on: Feb 14, 2016
 *      Author: fiveletter
 */

#ifndef L2_DRIVERS_GPIO_H_
#define L2_DRIVERS_GPIO_H_
#ifdef __cplusplus
extern "C" {
#endif
typedef struct gpio_info {
    uint8_t port;
    uint8_t pin;
    bool dir;
} gpio_info_S;

typedef enum gpio_pin {
    BOARD_LED = 0,
    SWITCH_IN,
	BREAD_LED,
	LED_6,
    GPIO_SPI1_CS
} gpio_pin_E;

/**
 * Initializes the gpio peripheral
 */
void gpio_init(void);

/**
 * Sets gpio
 * @param gpio_pin enumeration of specific GPIO pin
 */
void gpio_set(gpio_pin_E);

/**
 * Clears gpio
 * @param gpio_pin enumeration of specific GPIO pin
 */
void gpio_clear(gpio_pin_E);

/**
 * Get value of GPIO
 * @param gpio_pin enumeration of specific GPIO pin
 * @return current state of GPIO pin
 */

bool gpio_state(gpio_pin_E);

#ifdef __cplusplus
}
#endif
#endif /* L2_DRIVERS_GPIO_H_ */
