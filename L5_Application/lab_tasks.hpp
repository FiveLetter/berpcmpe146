#ifndef LAB_TASKS_HPP_
#define LAB_TASKS_HPP_

#include <stdio.h>
#include <math.h>

#include "acceleration_sensor.hpp"
#include "gpio.h"
#include "scheduler_task.hpp"
#include "semphr.h"
#include "shared_handles.h"
#include "uart1.h"

#define AcelSen      Acceleration_Sensor::getInstance() ///< Acceleration Sensor

typedef enum {
    invalid = 0,
    left,
    right,
    up,
    down
} orientation_t;

class orientCompute : public scheduler_task
{
    /*
        When priority is the same:
        LPC: Sending a value
        Sent a value to the queue
        orientation: 0
        Sending a value
        orientation: 0
        Sent a value to the queue
        Sending a value
        Seorientation: 0

        t a value to the queue
        Sending a value
        Seorientation: 0
        nt a value to the queue
        Sending a value
        Sorientation: 0

        t a value to the queue
        ...

        When receiving priority is higher
        LPC: Sending a value
        orientation: 0
        Sent a value to the queue
        Sending a value
        orietnation: 0
        Sent a value to the queue
        Sending a value
        orietnation: 0
        Sent a value to the queue
        ...
    */
    public:
        orientCompute(uint8_t priority):
            scheduler_task("orientCompute", 1000, priority)
            {
                QueueHandle_t acel_queue = xQueueCreate(1, sizeof(orientation_t));
                addSharedObject(shared_OrientQueue, acel_queue);
            }

            bool init(void)
            {
                return true;
            }

            bool run(void *p)
            {
                orientation_t orientation = invalid;
                int16_t x_val = AcelSen.getX(); ///< pos = left; neg = right
                int16_t y_val = AcelSen.getY(); ///< pos = down; neg = up
                int16_t deltaXY = abs(abs(y_val) - abs(x_val));
                
                 ///< if difference is not large enough leave invalid
                if (deltaXY > 50)
                {
                    if (abs(y_val) > abs(x_val)) 
                    {
                        orientation = (y_val > 0) ? down : up;
                    } 
                    else
                    {
                        orientation = (x_val > 0) ? left : right;
                    }
                }
                printf("Sending a value\n");
                xQueueSend(getSharedObject(shared_OrientQueue), &orientation, portMAX_DELAY);
                printf("Sent a value to the queue\n");
                vTaskDelay(1000);
                return true;
            }
};

class orientProcess : public scheduler_task
{
    public:
        orientProcess(uint8_t priority):
            scheduler_task("orientProcess", 1000, priority)
            {
                /* Nothing to init */
            }

            bool init(void)
            {
                return true;
            }

            bool run(void *p)
            {
                orientation_t orientation = invalid;
                QueueHandle_t qid = getSharedObject(shared_OrientQueue);

                if (xQueueReceive(qid, &orientation, portMAX_DELAY))
                {
                    /*
                    0 invalid    1 left    2 right    3 up    4 down
                    */
                    printf("orientation: %d\n", orientation);

                    if (orientation == left || orientation == right)
                    {
                        gpio_clear(LED_6);
                    }
                    else
					{
						gpio_set(LED_6);
					}
                }
                return true;
            }
};

class blinkLED : public scheduler_task
{
    public:
        blinkLED(uint8_t priority):
            scheduler_task("blinkLED", 1000, priority)
            {
                /* Nothing to init */
            }

            bool init(void)
            {
                return true;
            }

            bool run(void *p)
            {
                // static bool on = false;

                if (gpio_state(LED_6))
                {
                    gpio_clear(LED_6);
                }
                else
                {
                    gpio_set(LED_6);
                }

                vTaskDelay(500);
                return true;
            }

};

class gpioTask : public scheduler_task
{
	private:
		bool on = false;
    public:
        gpioTask(uint8_t priority) :
            scheduler_task("gpioTask", 2000, priority)
        {
            /* Nothing to init */
        }

        bool init(void)
        {
        	gpio_init();
        	return true;
        }
        bool run(void *p)
        {
        	if (gpio_state(SWITCH_IN)){
        		gpio_set(BREAD_LED);
        	} else {
        		gpio_clear(BREAD_LED);
        	}

        	vTaskDelay(500);
        	return true;
        }
};

class uartTx : public scheduler_task
{
    public:
    uartTx(uint8_t priority) :
            scheduler_task("uartTx", 2000, priority)
        {
            /* Nothing to init */
        }

        bool init(void)
        {
          return true;
        }
        bool run(void *p)
        {
          char input = 0;

          scanf("%c", &input);
          uart1_putchar((uint8_t)input);
          return true;
        }
};

class uartRx : public scheduler_task
{
    public:
    uartRx(uint8_t priority) :
            scheduler_task("uartRx", 2000, priority)
        {
            /* Nothing to init */
        }

        bool init(void)
        {
          return true;
        }
        bool run(void *p)
        {
          uint8_t data = 0;
          uart1_getchar(&data);
          printf("%c", data);
          return true;
        }
};

#endif /* LAB_TASKS_HPP_ */
